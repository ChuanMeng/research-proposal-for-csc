\section{Research objectives and methodology}

\subsection{Objectives}
The aim of this PhD program is to advance research into \ac{CIR} by building on the methods of relatively well-studied \ac{KGC}, i.e., applying \ac{KGC} methods to \ac{CIR}.
To achieve this aim, we have to additionally address the three \ac{CIR} challenges that existing \ac{KGC} methods do not tackle well yet~(predicting system actions, selecting information in long documents, and data scarcity), which correspond to the three clear and specific objectives we want to accomplish:

\begin{enumerate}[leftmargin=*,nosep,label=(\arabic*)]
\item \emph{Proposing a new method capable of predicting system actions}.
Specifically, we will first introduce a system action taxonomy including all the necessary system actions in \ac{CIR}, such as providing an answer, asking a clarifying question, recommending an item or greeting.
And then, we will design a method capable of predicting the appropriate system actions at the right time.
Note that the newly devised method should not depend on much domain expertise or manual annotation of actions.

\item \emph{Proposing a new method capable of effectively selecting information in long documents}.
Specifically, previous work on \ac{KGC} does not consider the intrinsic structure and relationships contained in long documents, like paragraph structure or the relationship between sentences.
We will utilize such intrinsic structure and relationships to help systems find the most appropriate part in a long document.

\item \emph{Proposing a new method capable of effectively overcoming data scarcity}.
Specifically, we can fully make use of abundant \ac{KGC} datasets to benefit \ac{CIR} by trying to decrease the impact of the corpus discrepancy between the two tasks.
\end{enumerate}

\subsection{Methodology}
To accomplish the above three objectives, we propose three novel specific solutions.
\begin{enumerate}[leftmargin=*,nosep,label=(\arabic*)]
\item \emph{Multi-agent learning based action prediction}.
We formulate action prediction as a reinforcement learning~\citep{sutton2000policy} problem, regard the system and user as two separate agents, and then train them to learn to decide their own actions in a multi-agent reinforcement learning way~\citep{hernandez2019survey}.
Concretely, the two agents are required to have a cooperative interactive
process where they decide their own actions, interact with each other and collaborate to meet the user's information needs.
Through this process, the two agents get their own rewards of their action decisions and they are trained together based on the rewards.
The interactive learning process not only requires no explicit expertise but also reduces the dependence on manual annotation of actions.

\item \emph{Hierarchical information selection}.
Building on ideas from the long document-based machine reading comprehension task~\citep{zhang2018towards} and the long document-based summarization task~\citep{gidiotis2020divide}, we leverage the intrinsic structure of long documents and divide long documents into paragraphs. 
Afterwards, we require our system to first find the most relevant paragraph and then attentively read the chosen paragraph to get the most appropriate part.

\item \emph{Meta-learning based domain adaptation}.
We first pretrain a model on abundant \ac{KGC} datasets~\citep{gopalakrishnan2019topical,dinan2018wizard}, and then we make the model adapt quickly to the \ac{CIR} task given only a few examples of \ac{CIR} with the help of meta learning~\citep{finn2017model,li2018learning,qian2019domain}, which not only substantially reduces the dependence on \ac{CIR} training sets but also decreases the impact of the corpus discrepancy between the two tasks.
\end{enumerate}
