\section{Literature review} \label{lr}
We survey two categories of related work: \acf{KGC} and \acf{CIR}.

\begin{figure}[t]
\centering
\includegraphics[width=1\columnwidth]{images/figure_1.pdf}
 \caption{Examples of the interaction between \ac{KGC} and \ac{CIR} systems and a user.}
 \label{f1}
\end{figure}


\subsection{Knowledge-grounded conversation}
\label{s2-1}
\ac{KGC} task has been studied for several years.
Generally, three sequential sub-tasks have to be accomplished~\citep{ghazvininejad2018knowledge,dinan2018wizard}:
\begin{enumerate*}[label=(\arabic*)]
\item \emph{conversational context and knowledge encoding}, that is, encoding the given conversational context and knowledge into latent representations;
\item \emph{knowledge selection}, that is, choosing the most appropriate knowledge to be incorporated in the next response based on the conversational context;
\item \emph{response generation}, that is, generating a response token by token based on the chosen knowledge and the conversational context. 
\end{enumerate*}
Existing \ac{KGC} methods can be categorised into two groups.
Methods in the first group leverage \textit{structured knowledge}~(knowledge graphs comprised of knowledge triples, e.g., Freebase) ~\citep{jung2020attnio,zhou2020kdconv,moon2019opendialkg,zhou2018commonsense}.
The ones in the second group leverage \textit{unstructured knowledge}~(a document or some separate pieces of knowledge, e.g., Wikipedia article or Foursquare tips)~\citep{meng2020dukenet,meng2020refnet,ren-2021-conversations,kim2019sequential,qin2019conversing,gopalakrishnan2019topical,dinan2018wizard,ghazvininejad2018knowledge, moghe2018towards}.

%For both groups, there are some key research directions:
%\begin{enumerate*}
%\item improving knowledge selection~\citep{kim2020sequential};
%\item improving knowledge-aware response generation~\citep{zhao2020knowledge}
%\item leveraging multiple knowledge modalities~\citep{zhao2020multiple};
%that is, how to utilise structured, unstructured and even other types of knowledge simultaneously;
%\item overcoming data scarcity~\citep{zhao2019low, li2020zero};
%that is, how to improve the performance of \ac{KGC} methods under a few- or zero-shot setting.
%\end{enumerate*}

\subsection{Conversational information retrieval}
\label{s2-2}
\ac{CIR} is increasingly attracting attention. It aims to meet users' information needs by directly conducting conversations between search engines and users.
Thus, \ac{CIR} systems need to be able to directly generate a response to users, instead of just returning a ranked list of retrieved documents like traditional \ac{IR} systems.
%Generally, systems of \ac{CIR} also have three sequential tasks to finish~\citep{ren-2021-conversations}:
%\begin{enumerate*}[label=(\arabic*)]
%\item \emph{conversation context and document encoding}, that is, encoding the given conversation context and documents into latent representations;
%\item \emph{document selection}, that is, choosing the most relevant document to be incorporated in the next response based on the conversation context;
%\item \emph{response generation}, that is, generating a response token by token based on the chosen document and conversation context. 
%\end{enumerate*}
However, due to a lack of practical formalisms or suitable resources for \ac{CIR}, related work mainly focuses on user studies~\citep{trippas2018informing,vtyurina2017exploring}, theoretical/conceptual frameworks~\citep{radlinski2017theoretical,azzopardi2018conceptualizing}, or addressing a specific aspect of \ac{CIR}, e.g., recommending items to users~\citep{sun2018conversational}, asking clarifying questions to users~\citep{zamani2020generating,aliannejadi2019asking}, just returning the most relevant passages for users' questions~\citep{voskarides2020query}, or returning answers for users' questions~\citep{ren-2021-conversations}.
No \ac{CIR} systems has accomplished the goal of conducting natural conversations with users, and there are so many important issues not well-studied in the scenario of \ac{CIR}.

\subsection{Discussion}
Based on \S\ref{s2-1}, \S\ref{s2-2} and Fig.~\ref{f1}, we infer that unstructured knowledge based \ac{KGC} methods have a very close connection with \ac{CIR}.
The documents retrieved by search engines in \ac{CIR} can be regarded as a sort of unstructured knowledge.
Besides, \ac{KGC} and \ac{CIR} have almost the same task definition, i.e., both condition response generation on knowledge~(documents) and conversation context.
Therefore, based on the connection between them and the current status of research into \ac{CIR}, it is natural to try to advance research into \ac{CIR} by building on advances in \ac{KGC}.
However, some important \ac{CIR} challenges, cannot be addressed by existing \ac{KGC} methods:

\begin{enumerate}[leftmargin=*,nosep,label=(\arabic*)]
\item Predicting system actions.
Systems for \ac{CIR} should take appropriate actions~(e.g., providing an answer, asking a clarifying question, recommending an item, or greeting) at the right time, which is necessary to \ac{CIR}~\citep{radlinski2017theoretical} but has not yet been put into practice.
For this challenge, existing \ac{KGC} methods have not considered modeling system actions so far.
Besides, just using rule-based and data-driven methods to predict system actions suffers from the problem of requiring much expertise and manual annotation of actions, respectively.

\item Selecting information in long documents.
It is common that the documents retrieved by search engines are long web-page documents containing too much redundant information, which makes it hard for systems to find the most appropriate part in a long document.
For this challenge, the existing \ac{KGC} methods either regard a long document as a long text and directly filter it~\citep{moghe2018towards}, or truncate a long document into independent sentences and then select the most suitable one~\citep{dinan2018wizard,gopalakrishnan2019topical}.
The former ignores the paragraph structure in long documents, and the latter ignores the relationship between sentences, all limiting the performance.

\item Data scarcity.
There is a lack of large-scale training sets for \ac{CIR} because the \ac{CIR} task is still a new paradigm and manually labelling is resource intensive~\citep{ren-2021-conversations}.
Only considering existing \ac{CIR} datasets as training sets is far from enough for \ac{KGC} methods to achieve a satisfactory performance.
Moreover, simply applying \ac{KGC} methods trained on \ac{KGC} datasets to \ac{CIR} cannot achieve good results due to a discrepancy between the corpora for the two tasks. 
\end{enumerate}

\noindent
Thus, this PhD program aims to propose new \ac{CIR} methods that not only absorb the advantages of relatively well-studied \ac{KGC} methods but also are capable of handling the three important \ac{CIR} challenges in order to speed up the process of putting \ac{CIR} into practice.


